# Port of this pygame tetris https://gist.github.com/silvasur/565419
# Copyright (c) 2010 "Laria Carolin Chabowski"<me@laria.me>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
import leds

log = logging.Log(__name__, level=logging.INFO)
log.info("import")

from random import randrange as rand

# The configuration
config = {
    'cell_size': 6,
    'cols':      8,
    'rows':      16,
    'delay':     750,
}

colors = [
(0,   0,   0  ),
(255, 0,   0  ),
(0,   150, 0  ),
(0,   0,   255),
(255, 120, 0  ),
(255, 255, 0  ),
(180, 0,   255),
(0,   220, 220)
]

# Define the shapes of the single parts
tetris_shapes = [
    [[1, 1, 1],
     [0, 1, 0]],
    
    [[0, 2, 2],
     [2, 2, 0]],
    
    [[3, 3, 0],
     [0, 3, 3]],
    
    [[4, 0, 0],
     [4, 4, 4]],
    
    [[0, 0, 5],
     [5, 5, 5]],
    
    [[6, 6, 6, 6]],
    
    [[7, 7],
     [7, 7]]
]

def rotate_clockwise(shape):
    return [ [ shape[y][x]
            for y in range(len(shape)) ]
        for x in range(len(shape[0]) - 1, -1, -1) ]

def check_collision(board, shape, offset):
    off_x, off_y = offset
    for cy, row in enumerate(shape):
        for cx, cell in enumerate(row):
            try:
                if cell and board[ cy + off_y ][ cx + off_x ]:
                    return True
            except IndexError:
                return True
    return False

def remove_row(board, row):
    del board[row]
    return [[0 for i in range(config['cols'])]] + board
    
def join_matrixes(mat1, mat2, mat2_off):
    off_x, off_y = mat2_off
    for cy, row in enumerate(mat2):
        for cx, val in enumerate(row):
            mat1[cy+off_y-1    ][cx+off_x] += val
    return mat1

def new_board():
    board = [ [ 0 for x in range(config['cols']) ]
            for y in range(config['rows']) ]
    board += [[ 1 for x in range(config['cols'])]]
    return board

class TetrisApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.gameover = False
        self.paused = False
        self._delta_ms_sum = 0
        self.score = 0.0

        self.width = config['cell_size']*config['cols']
        self.height = config['cell_size']*config['rows']
        
        self.init_game()

    def new_stone(self):
        self.stone = tetris_shapes[rand(len(tetris_shapes))]
        self.stone_x = int(config['cols'] / 2 - len(self.stone[0])/2)
        self.stone_y = 0
        
        if check_collision(self.board,
                           self.stone,
                           (self.stone_x, self.stone_y)):
            self.gameover = True
    
    def draw_matrix(self, ctx, matrix, offset):
        off_x, off_y  = offset
        for y, row in enumerate(matrix):
            for x, val in enumerate(row):
                if val:
                    red, green, blue = colors[val]
                    ctx.rgb(red, green, blue,
                        ).rectangle(
                            (off_x+x) * config['cell_size'],
                            (off_y+y) * config['cell_size'], 
                            config['cell_size'],
                            config['cell_size'],
                        ).fill()

    def move(self, delta_x):
        if not self.gameover and not self.paused:
            new_x = self.stone_x + delta_x
            if new_x < 0:
                new_x = 0
            if new_x > config['cols'] - len(self.stone[0]):
                new_x = config['cols'] - len(self.stone[0])
            if not check_collision(self.board,
                                   self.stone,
                                   (new_x, self.stone_y)):
                self.stone_x = new_x
    
    def add_score(self):
        self.score += 1.0
        leds.set_all_hsv(self.score, 1.0, 1.0)
        leds.update()

    def drop(self):
        if not self.gameover and not self.paused:
            self.stone_y += 1
            if check_collision(self.board,
                               self.stone,
                               (self.stone_x, self.stone_y)):
                self.board = join_matrixes(
                  self.board,
                  self.stone,
                  (self.stone_x, self.stone_y))
                self.new_stone()
                while True:
                    for i, row in enumerate(self.board[:-1]):
                        if 0 not in row:
                            self.board = remove_row(
                              self.board, i)
                            self.add_score()
                            break
                    else:
                        break

    def rotate_stone(self):
        if not self.gameover and not self.paused:
            new_stone = rotate_clockwise(self.stone)
            if not check_collision(self.board,
                                   new_stone,
                                   (self.stone_x, self.stone_y)):
                self.stone = new_stone
    
    def toggle_pause(self):
        self.paused = not self.paused
    
    def start_game(self):
        if self.gameover:
            self.init_game()
            self.gameover = False

    def init_game(self):
        self.board = new_board()
        self.new_stone()
        self.score = 0.0
    
    def moveOrSkip(self, delta_ms, delay):
        self._delta_ms_sum += delta_ms
        if (self._delta_ms_sum > delay):
            self._delta_ms_sum = 0
            return True
        else:
            return False

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        if self.gameover:
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 30
            ctx.rgb(1, 1, 1)
            ctx.move_to(0, 0)
            ctx.save()
            ctx.text("GAME OVER")
            ctx.restore()

        # Game board
        ctx.scale(2, 2).translate(-25, -50)
        ctx.rgb(1, 1, 1).rectangle(0, 0, self.width, self.height).stroke()
        
        # Draw the game
        self.draw_matrix(ctx, self.board, (0, 0))
        self.draw_matrix(ctx, self.stone,
                            (self.stone_x,
                            self.stone_y))

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        # App wheel for movment
        if self.input.buttons.app.middle.pressed:
            self.rotate_stone()
        elif self.input.buttons.app.left.pressed:
            self.move(-1)
        elif self.input.buttons.app.right.pressed:
            self.move(1)

        # Top Right
        if ins.captouch.petals[2].pressed:
            if not self.moveOrSkip(delta_ms, (config['delay'] / 10)):
                return
            self.drop()
        # Top Middle
        elif ins.captouch.petals[0].pressed:
            self.start_game()

        # Tick to move the block down
        if not self.moveOrSkip(delta_ms, config['delay']):
            return

        self.drop()


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(TetrisApp(ApplicationContext()))